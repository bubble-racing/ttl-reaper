FROM bitnami/kubectl:1.23

# Copy in reaper script
COPY ./ttl-reaper.sh /home/reaper/ttl-reaper.sh
ENTRYPOINT [ "bash", "/home/reaper/ttl-reaper.sh" ]