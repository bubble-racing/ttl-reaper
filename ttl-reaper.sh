# exit on error
set -e

# expected environment variables
# namespaces example: 'dev qa stage'
# resource_types example: 'jobs service'

# Reap all of a given resource type in a namespace
reap_group () {
  namespace=$1
  resource_type=$2

  names=$(kubectl get $resource_type -n $namespace -l ttl -o=jsonpath='{.items.*.metadata.name}')
  echo "Found the following $resource_type resources in namespace $namespace with a ttl set: $names"

  for name in $names; do
    reap_resource $namespace $resource_type $name
  done
}

# Reap a single resource, only if its ttl is expired
reap_resource () {
  namespace=$1
  resource_type=$2
  name=$3

  ttl=$(kubectl get $resource_type $name -n $namespace -o=jsonpath='{.metadata.labels.ttl}')
  creation_timestamp=$(kubectl get $resource_type $name -n $namespace -o=jsonpath='{.metadata.creationTimestamp}')
  creation_seconds=$(date -d "$creation_timestamp" +"%s")
  expire_seconds=$(($creation_seconds + $ttl))
  current_seconds=$(date +"%s")
  if [[ $expire_seconds -lt $current_seconds ]]; then
    echo "Deleting $resource_type $name in namespace $namespace"
    kubectl delete $resource_type $name -n $namespace
  fi
}

# Reap the given namespaces and resource types
echo "ttl-reaper has awoken"
echo "namespaces: $namespaces"
echo "resource_types: $resource_types"
for namespace in $namespaces; do
  for resource_type in $resource_types; do
    reap_group $namespace $resource_type
  done
done
